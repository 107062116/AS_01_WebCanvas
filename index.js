// Canvas DOM 元素 
var canvas = document.getElementById('canvas');
var ctx = canvas.getContext('2d');
var screen = document.getElementById("screen");
var fontSize = document.getElementById('fontsizeSelect');
var fontFace = document.getElementById('selectFont');
//var cursor = canvas.style.cursor;
var undoArray = new Array();
var Step = -1;
// var redoArr = new Array();


function Radio_3px_onclick(){ctx.lineWidth = 3;}
function Radio_5px_onclick(){ctx.lineWidth = 5;console.log(ctx.lineWidth);}
function Radio_10px_onclick(){ctx.lineWidth = 10;console.log(ctx.lineWidth)}
function Radio_20px_onclick(){ctx.lineWidth = 20;console.log(ctx.lineWidth)}
function Radio_40px_onclick(){ctx.lineWidth = 40;console.log(ctx.lineWidth)}
function Radio_70px_onclick(){ctx.lineWidth = 70;console.log(ctx.lineWidth)}

//起始點座標
// let x1 = 0
// let y1 = 0
var flag = 0
//var fiag_solid = 1
// // 終點座標
// let x2 = 0
// let y2 = 0
// 宣告一個 hasTouchEvent 變數，來檢查是否有 touch 的事件存在
var hasTouchEvent = 'ontouchstart' in window ? true : false

// 透過上方的 hasTouchEvent 來決定要監聽的是 mouse 還是 touch 的事件
var downEvent = hasTouchEvent ? 'ontouchstart' : 'mousedown'
var moveEvent = hasTouchEvent ? 'ontouchmove' : 'mousemove'
var upEvent = hasTouchEvent ? 'touchend' : 'mouseup'
// 宣告 isMouseActive 為滑鼠點擊的狀態，因為我們需要滑鼠在 mousedown 的狀態時，才會監聽 mousemove 的狀態
let isMouseActive = false

function pencil(){
    
    ctx.globalCompositeOperation = "source-over";
    document.getElementById("canvas").style.cursor = 'url(pencil.png), auto';
    flag = 1;
    draw();
    
}

function eraser(){
    ctx.globalCompositeOperation = "destination-out";
    document.getElementById("canvas").style.cursor = 'url(eraser_cursor.png), auto';
    flag = 2;
    draw_eraser();
}
function text(){
  ctx.globalCompositeOperation = "source-over";  
  document.getElementById("canvas").style.cursor = 'text';
  flag = 6;
    type();
}

function circle(){
    document.getElementById("canvas").style.cursor = 'crosshair';
    ctx.globalCompositeOperation = "source-over";
    flag = 3;
    draw_circle();
}

function rectangle(){
    document.getElementById("canvas").style.cursor = 'crosshair';
    ctx.globalCompositeOperation = "source-over";
    flag = 4;
    draw_rectangle();
}

function triangle(){
    document.getElementById("canvas").style.cursor = 'crosshair';
    ctx.globalCompositeOperation = "source-over";
    flag = 5;
    draw_triangle();
}
//reset
document.getElementById('reset').addEventListener('click', function() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    stock()
  }, false);

function draw(){
    canvas.addEventListener(downEvent, function(e) {
        isMouseActive = true
    })

    canvas.addEventListener(downEvent, function(e) {
        isMouseActive = true
        x1 = e.offsetX
        y1 = e.offsetY

        //ctx.lineWidth = 5
        ctx.lineCap = 'round'
        ctx.lineJoin = 'round'
        ctx.beginPath()
    })

    canvas.addEventListener(moveEvent, function(e) {
        if (!isMouseActive) {
            return
        }
        // 取得終點座標
        x2 = e.offsetX
        y2 = e.offsetY

        //ctx.lineWidth = ;
        //console.log(ctx.lineWidth);

        // 開始繪圖
        //ctx.beginPath()
        if(flag===1){
          ctx.moveTo(x1, y1)
          ctx.lineTo(x2, y2)
          ctx.globalCompositeOperation = "source-over";
          ctx.stroke()
        }
        // 更新起始點座標
        x1 = x2
        y1 = y2
        
    })

    canvas.addEventListener(upEvent, function(e) {
        isMouseActive = false
        ctx.globalCompositeOperation = "source-over";
        if(flag===1) stock();
    })
}

function draw_eraser(){
  canvas.addEventListener(downEvent, function(e) {
      isMouseActive = true
  })

  canvas.addEventListener(downEvent, function(e) {
      isMouseActive = true
      x1 = e.offsetX
      y1 = e.offsetY

      //ctx.lineWidth = 5
      ctx.lineCap = 'round'
      ctx.lineJoin = 'round'
      ctx.beginPath()
  })

  canvas.addEventListener(moveEvent, function(e) {
      if (!isMouseActive) {
          return
      }

      // 取得終點座標
      x2 = e.offsetX
      y2 = e.offsetY

      //ctx.lineWidth = ;
      //console.log(ctx.lineWidth);

      // 開始繪圖
      //ctx.beginPath()
      if(flag===2){
        ctx.moveTo(x1, y1)
        ctx.lineTo(x2, y2)
        ctx.globalCompositeOperation = "destination-out";
        ctx.stroke()
    }
      // 更新起始點座標
      x1 = x2
      y1 = y2
      
  })

  canvas.addEventListener(upEvent, function(e) {
      isMouseActive = false
      ctx.globalCompositeOperation = "source-over";
      if(flag==2) stock();
  })
}

function draw_rectangle(){
  if(flag!=4){
    return;
  }
  canvas.addEventListener(downEvent, function(e) {
      isMouseActive = false
  })

  canvas.addEventListener(downEvent, function(e) {
      isMouseActive = false
      x1 = e.offsetX
      y1 = e.offsetY

      //ctx.lineWidth = 5
      ctx.lineCap = 'round'
      ctx.lineJoin = 'round'
      ctx.beginPath()
  })

  
  
    canvas.addEventListener(upEvent, function(e) {
      // 取得終點座標
      x2 = e.offsetX
      y2 = e.offsetY

      if(flag===4){
        // 開始繪圖
        //ctx.beginPath()
        ctx.moveTo(x1, y1)
        ctx.lineTo(x2, y1)
        //ctx.globalCompositeOperation = "source-over";
        ctx.stroke()

        ctx.moveTo(x2, y1)
        ctx.lineTo(x2, y2)
        ctx.stroke()

        ctx.moveTo(x2, y2)
        ctx.lineTo(x1, y2)
        ctx.stroke()

        ctx.moveTo(x1, y2)
        ctx.lineTo(x1, y1)
        ctx.stroke()
        if(flag===4) stock();
      }
      isMouseActive = false
      //stock();
    })
  
}

function draw_triangle(){
  if(flag!=5){
    return;
  }
  canvas.addEventListener(downEvent, function(e) {
    isMouseActive = false
})

canvas.addEventListener(downEvent, function(e) {
    isMouseActive = false
    x1 = e.offsetX
    y1 = e.offsetY

    //ctx.lineWidth = 5
    // ctx.lineCap = 'round'
    // ctx.lineJoin = 'round'
    ctx.beginPath()
})



  canvas.addEventListener(upEvent, function(e) {
    // 取得終點座標
    x2 = e.offsetX
    y2 = e.offsetY

    if(flag==5){
      // 開始繪圖
      //ctx.beginPath()
      ctx.moveTo(x1, y1)
      ctx.lineTo(x2, y2)
      //ctx.globalCompositeOperation = "source-over";
      ctx.stroke()

      ctx.moveTo(x2, y2)
      ctx.lineTo(2*x1-x2, y2)
      ctx.stroke()

      ctx.moveTo(2*x1-x2, y2)
      ctx.lineTo(x1, y1)
      ctx.stroke()
      if(flag===5) stock();
    }
    isMouseActive = false
    
  })
  
}

function draw_circle(){
  if(flag!=3){
    return;
  }
  canvas.addEventListener(downEvent, function(e) {
    isMouseActive = false
})

canvas.addEventListener(downEvent, function(e) {
    isMouseActive = false
    x1 = e.offsetX
    y1 = e.offsetY

    //ctx.lineWidth = 5
    ctx.lineCap = 'round'
    ctx.lineJoin = 'round'
    ctx.beginPath()
})



  canvas.addEventListener(upEvent, function(e) {
    // 取得終點座標
    x2 = e.offsetX
    y2 = e.offsetY

    if(flag==3){
      console.log("circle");
      
      ctx.arc((x1+x2)/2, (y1+y2)/2 , (Math.sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2)))/2, 0 , Math.PI*2 ,false );
      ctx.stroke()
      if(flag===3) stock();
      //console.log(flag_solid)
    }
    isMouseActive = false
  })

}

function type(){
  // ctx.font = `${fontsizeSelect.value}px ${fontFace.options[fontFace.selectedIndex].value}`;
  // ctx.lineWidth = 1;

  canvas.addEventListener(downEvent, function(e) {
    x1 = e.offsetX
    y1 = e.offsetY
    //ctx.font="size Arial";
    ctx.font = `${fontSize.value} ${fontFace.options[fontFace.selectedIndex].value}`;
    if(flag===6) ctx.lineWidth = 5;
    ctx.fillText(screen.value, x1, y1);
    screen.value=" "
    if(flag===6) stock();
})

}

document.getElementById('color').onchange=function(){
  ctx.strokeStyle=this.value
}

function undo() {
  if (Step > -1) {
      Step--;
      var canvasPic = new Image();
      canvasPic.src = undoArray[Step];
      ctx.clearRect(0, 0, canvas.width, canvas.height);
      canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
      console.log(Step)
  }
  console.log("undo")
}

function redo() {
  if (Step < undoArray.length-1) {
      Step++;
      var canvasPic = new Image();
      canvasPic.src = undoArray[Step];
      ctx.clearRect(0, 0, canvas.width, canvas.height);
      canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
      console.log(Step)
  }
  console.log("redo")
}

document.getElementById('files').addEventListener('change', onload, false);

function stock(){
  Step++;
  if (Step < undoArray.length) { undoArray.length = Step; }
  console.log(Step)
  undoArray.push(document.getElementById('canvas').toDataURL());
  
}

document.getElementById('files').onchange = function(e) {
  var img = new Image();
  img.onload = onload;
  img.onerror = failed;
  img.src = URL.createObjectURL(this.files[0]);
  
};
function onload() {
  
  ctx.drawImage(this, 10,10);
  stock();
}
function failed() {
  console.error("The provided file couldn't be loaded as an Image media");
}
