# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |


---
![](https://i.imgur.com/KPOs3aX.png)

### How to use 

    調整粗細：
    在右側的工具欄裡，有5種brush size可以選。
    
    筆刷：
    按下筆刷圖示可以根據筆觸繪製線段。
    
    橡皮擦：
    按下Eraser圖示可以根據筆觸清除。
    
    下載：
    按下畫面左上角的Download即可下載現在圖片。
    
    清除：
    按下畫面左上角的Reset即可清除畫布。
    
    Undo/Redo：
    按下Undo圖示回到上一步，按下Redo圖示到下一步。
    
    調色盤：
    按下想要的顏色就可以換筆刷顏色！
    
    圖片上傳：
    點選左上角的選擇檔案即可onload。
    
    輸入文字：
    可在畫面右側的工具欄裡選擇字型與字體大小，接著於工具欄裡的輸入欄位輸入，
    完成上述步驟後按下文字圖示再點擊canvas便會於點擊位置出現輸入的文字。
    
    繪製圖形：
    在工具欄中點選不同圖示並於畫布上點擊拖曳放開後便可繪出圖形。


### Function description

    Decribe your bouns function and how to use it.
    

### Gitlab page link

https://107062116.gitlab.io/AS_01_WebCanvas

### Others (Optional)

    Anythinh you want to say to TAs.

<style>
table th{
    width: 100%;
}
</style>